#Asterisk installation

#! /bin/bash

# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

#update the system first
echo "Updating system"
sleep 1
apt update && sudo apt -y upgrade

#Install Asterisk 18 LTS dependencies
sleep 2

echo "installing Dependenices"
sleep 2

apt -y install git curl wget libnewt-dev libssl-dev libncurses5-dev subversion  libsqlite3-dev build-essential libjansson-dev libxml2-dev  uuid-dev
apt -y install lsof telnet tree screen htop sngrep

#If you get an error for subversion package on Ubuntu like below:
#E: Package 'subversion' has no installation candidate
#Then add universe repository and install subversion from it:
sleep 2
echo "add-apt-repository universe"
sleep 2
apt update && sudo apt -y install subversion
sleep 2
#Download Asterisk 20 LTS tarball
#Download the latest release of Asterisk # LTS to your local system for installation.
echo"going inside /usr/src/ directory"
sleep 1
cd /usr/src/
sleep 2
echo "Downloading Asterisk"
sleep 1
curl -O http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-20-current.tar.gz
sleep 2
echo "Extracting File"
sleep 2
tar xvf asterisk-20-current.tar.gz

echo "inside asterisk directory"
cd asterisk-20.*
sleep 2
#/Run the following command to download the mp3 decoder library into the source tree.

echo "Downloading mp3 decoder library into source tree"
sleep 2
contrib/scripts/get_mp3_source.sh
sleep 2
echo "Ensuring all dependencies are resolved"
sleep 2
contrib/scripts/install_prereq install

#You should get a success message at the end.

#Build and Install Asterisk

sleep 2
echo "Building and installing asterisk"
sleep 2
#Run the configure script to satisfy build dependencies.
./configure

#Setup menu options by running the following command
sleep 2
echo "Select menu options"
sleep 2
make menuselect

#You can select configurations you see fit. When done, save and exit then install Asterisk with selected modules.
sleep 2
echo "Building asterisk"
sleep 2
make -j3
sleep 2
echo "Installing asterisk"
sleep 2
make install
sleep2
echo "Installing configs and samples"
sleep 2
make samples
make config
ldconfig
sleep 2
echo "starting asterisk service"
sleep 2
systemctl start asterisk
sleep 2
#Create a separate user and group to run asterisk services, and assign correct permissions

echo "Creating a separate user and group to run asterisk services and assiging correct permissions"
sleep 2
groupadd asterisk
useradd -r -d /var/lib/asterisk -g asterisk asterisk
usermod -aG audio,dialout asterisk
chown -R asterisk.asterisk /etc/asterisk
chown -R asterisk.asterisk /var/{lib,log,spool}/asterisk
chown -R asterisk.asterisk /usr/lib/asterisk
sleep 2
echo "Edit the file with the following
##AST_USER="asterisk"
##AST_GROUP="asterisk"
"
sleep 3
nano /etc/default/asterisk

sleep 2

echo "Edit the file with the following
#runuser = asterisk ; The user to run as.
#rungroup = asterisk ; The group to run as."
sleep 3
nano /etc/asterisk/asterisk.conf

sleep 2
echo "Configuring Start Script"

cp /usr/src/asterisk-20.*/contrib/init.d/rc.debian.asterisk /etc/init.d/asterisk
sleep 2
echo "edit the copied rc.debian.asterisk file with.

#Full path to asterisk binary
#DAEMON=/usr/sbin/asterisk (type asterisk command is used to get the asterisk directory)
#ASTVARRUNDIR=/var/run/asterisk
#ASTETCDIR=/etc/asterisk
#TRUE=/bin/true."
sleep 3
nano /etc/init.d/asterisk

#To run asterisk type asterisk -r
echo "Enabling asterisk service to start on boot"
sleep 2
systemctl enable asterisk

echo -e "\n\n\n\n"
echo "**********************************************************"
echo "Installation done"
echo "**********************************************************"
echo "connecting to asterisk"

sleep 2
asterisk -rvv

#If you have an active ufw firewall, open http ports and ports 5060,5061:
